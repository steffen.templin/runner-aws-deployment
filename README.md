This repository contains deployment scripts to set up a GitLab Runner
on AWS EC2. It uses a single EC2 instance with a local Docker executor.

Infrastructure is provisioned using Terraform. The EC2 runner instance
is configured using Ansible.

## Prerequisites

* Install aws-cli and configure the default profile (`aws configure`).
* Install terraform and ansible


## Deploy infrastructure

Provide the following variables:

* `ssh_key_name`: Name of the AWS key-pair to use for accessing the EC2 instance
* `ssh_key_path`: Path to the SSH public key file


Set-up terraform:

    cd terraform/
    terraform init


Deploy:

    terraform apply


## Deploy software

Create inventory file `ansible/hosts`:

    [runners]
    <public-dns-from-terraform-output> ansible_user=ec2-user ansible_become=yes

Test conncetion:

    ansible runners -i hosts -m ping

Install docker and runner:

    ansible-playbook -i hosts site.yml

Unfortunately, the gitlab_runner ansible module seems out of date,
so we need to register the runner manually for now. In GitLab web,
go to Settings > CI/CD and expand the Runners section. Get the 
registration token. Set in the token and project name in belows
snippet and execute it after SSHing into the EC2 node:

    sudo gitlab-runner register \
        --non-interactive \
        --url "https://gitlab.com/" \
        --registration-token "<token-from-project-settings>" \
        --executor "docker" \
        --docker-image alpine:latest \
        --description "<project-name> runner" \
        --tag-list "docker,aws" \
        --run-untagged="true" \
        --locked="false" \
        --access-level="not_protected"

