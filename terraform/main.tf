module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "runner"
  cidr = "10.0.0.0/16"

  azs             = ["eu-central-1a"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = true
  enable_dns_hostnames = true
}

resource "aws_security_group" "runner_instance" {
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "SSH from world"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${chomp(data.http.icanhazip.body)}/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

module "runner_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "runner"

  ami                    = data.aws_ami.amzn2.id
  instance_type          = "t3.micro"
  key_name               = aws_key_pair.deployer.key_name
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.runner_instance.id]
  subnet_id              = module.vpc.public_subnets[0]

  associate_public_ip_address = true
}

resource "aws_key_pair" "deployer" {
  key_name   = var.ssh_key_name
  public_key = file(var.ssh_key_path)
}

data "aws_ami" "amzn2" {
  owners = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

data "http" "icanhazip" {
   url = "http://icanhazip.com"
}

output "runner_ip" {
  value = module.runner_instance.public_ip
}

output "runner_dns" {
  value = module.runner_instance.public_dns
}
