terraform {
  backend "s3" {
    bucket = "stemplin-tfstates-dev"
    key    = "terraform/state/runner-aws-deployment"
    region = "eu-central-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = var.aws_profile
  region = "eu-central-1"
}
