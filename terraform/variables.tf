variable "aws_profile" {
    type = string
    default = "default"
}

variable "ssh_key_name" {
    type = string
}

variable "ssh_key_path" {
    type = string
}
